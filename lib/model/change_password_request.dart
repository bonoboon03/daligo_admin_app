class ChangePasswordRequest {
  String changePassword;
  String changePasswordRe;
  String currentPassword;

  ChangePasswordRequest(this.currentPassword, this.changePassword, this.changePasswordRe);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['changePassword'] = changePassword;
    data['changePasswordRe'] = changePasswordRe;
    data['currentPassword'] = currentPassword;

    return data;
  }
}