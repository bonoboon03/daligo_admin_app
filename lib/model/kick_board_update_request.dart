class KickBoardUpdateRequest {
  double posX;
  double posY;
  bool isUse;

  KickBoardUpdateRequest(this.posX, this.posY, this.isUse);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['posX'] = posX;
    data['posY'] = posY;
    data['isUse'] = isUse;

    return data;
  }
}