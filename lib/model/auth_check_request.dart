class AuthCheckRequest {
  String phoneNumber;
  String authNumber;

  AuthCheckRequest(this.phoneNumber, this.authNumber);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['phoneNumber'] = phoneNumber;
    data['authNumber'] = authNumber;

    return data;
  }
}