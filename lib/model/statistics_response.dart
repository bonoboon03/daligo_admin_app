class StatisticsResponse {
  num totalPrice;

  StatisticsResponse(this.totalPrice);

  factory StatisticsResponse.fromJson(Map<String, dynamic> json) {
    return StatisticsResponse(
        json['totalPrice']
    );
  }
}