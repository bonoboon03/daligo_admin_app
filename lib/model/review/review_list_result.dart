import 'package:daligo_admin_app/model/review/review_item.dart';

class ReviewListResult {
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<ReviewItem>? list;

  ReviewListResult(this.totalItemCount, this.totalPage, this.currentPage, {this.list});

  factory ReviewListResult.fromJson(Map<String, dynamic> json) {
    return ReviewListResult(
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      list: json['list'] != null ? (json['list'] as List).map((e) => ReviewItem.fromJson(e)).toList() : [],
    );
  }
}