class ReviewItem {
  int id;
  String title;
  String name;
  String dateWrite;

  ReviewItem(this.id, this.title, this.name, this.dateWrite);

  factory ReviewItem.fromJson(Map<String, dynamic> json) {
    return ReviewItem(
      json['id'],
      json['title'],
      json['name'],
      json['dateWrite'],
    );
  }
}