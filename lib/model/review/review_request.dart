class ReviewRequest {
  String name;
  String title;
  String imgName;
  String content;

  ReviewRequest(this.name, this.title, this.imgName, this.content);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['name'] = name;
    data['title'] = title;
    data['imgName'] = imgName;
    data['content'] = content;

    return data;
  }
}