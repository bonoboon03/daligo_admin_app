class KickBoardResponse {
  String kickBoardStatus;
  String dateBuy;
  double posX;
  double posY;
  String isUse;

  KickBoardResponse(this.kickBoardStatus, this.dateBuy, this.posX, this.posY, this.isUse);

  factory KickBoardResponse.fromJson(Map<String, dynamic> json) {
    return KickBoardResponse(
      json['kickBoardStatus'],
      json['dateBuy'],
      json['posX'],
      json['posY'],
      json['isUse'],
    );
  }
}