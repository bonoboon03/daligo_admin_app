class KickBoardItem {
  int id;
  String uniqueNumber;
  String modelName;
  String kickBoardStatus;

  KickBoardItem(this.id, this.uniqueNumber, this.modelName, this.kickBoardStatus);

  factory KickBoardItem.fromJson(Map<String, dynamic> json) {
    return KickBoardItem(
      json['id'],
      json['uniqueNumber'],
      json['modelName'],
      json['kickBoardStatus'],
    );
  }
}