class FindPasswordRequest {
  String username;
  String name;
  String phoneNumber;

  FindPasswordRequest(this.username, this.name, this.phoneNumber);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['username'] = username;
    data['name'] = name;
    data['phoneNumber'] = phoneNumber;

    return data;
  }
}