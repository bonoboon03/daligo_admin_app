import 'package:daligo_admin_app/model/member/member_item.dart';

class MemberListResult {
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<MemberItem>? list;

  MemberListResult(this.totalItemCount, this.totalPage, this.currentPage, {this.list});

  factory MemberListResult.fromJson(Map<String, dynamic> json) {
    return MemberListResult(
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      list: json['list'] != null ? (json['list'] as List).map((e) => MemberItem.fromJson(e)).toList() : [],
    );
  }
}