import 'package:bot_toast/bot_toast.dart';
import 'package:daligo_admin_app/components/common/component_appbar_popup.dart';
import 'package:daligo_admin_app/components/common/component_custom_loading.dart';
import 'package:daligo_admin_app/components/common/component_margin_vertical.dart';
import 'package:daligo_admin_app/components/common/component_notification.dart';
import 'package:daligo_admin_app/components/common/component_text_btn.dart';
import 'package:daligo_admin_app/config/config_form_validator.dart';
import 'package:daligo_admin_app/config/config_style.dart';
import 'package:daligo_admin_app/enums/enum_size.dart';
import 'package:daligo_admin_app/model/review/review_request.dart';
import 'package:daligo_admin_app/pages/page_main.dart';
import 'package:daligo_admin_app/repository/repo_review.dart';
import 'package:daligo_admin_app/styles/style_form_decoration.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PageReviewUpdate extends StatefulWidget {
  const PageReviewUpdate({super.key, required this.id});

  final int id;

  @override
  State<PageReviewUpdate> createState() => _PageReviewUpdateState();
}

class _PageReviewUpdateState extends State<PageReviewUpdate> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _putReview(ReviewRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoReview().putReview(widget.id, request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '후기 수정 완료',
        subTitle: '후기 수정이 완료되었습니다.',
      ).call();
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => const PageMain()),
              (route) => false);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '후기 수정 실패',
        subTitle: '입력값을 확인해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(
        title: '후기 수정',
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return ListView(
      children: [
        const ComponentMarginVertical(),
        Container(
          padding: const EdgeInsets.only(
            bottom: 10,
            left: 20,
            right: 20,
          ),
          child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'name',
                    decoration: StyleFormDecoration().getInputDecoration('이름'),
                    maxLength: 20,
                    keyboardType: TextInputType.text,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(2, errorText: formErrorMinLength(2)),
                      FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'title',
                    decoration: StyleFormDecoration().getInputDecoration('제목'),
                    maxLength: 30,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(2, errorText: formErrorMinLength(2)),
                      FormBuilderValidators.maxLength(30, errorText: formErrorMaxLength(30)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'imgName',
                    decoration: StyleFormDecoration().getInputDecoration('이미지주소'),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'content',
                    decoration: StyleFormDecoration().getInputDecoration('내용'),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(10, errorText: formErrorMinLength(10)),
                    ]),
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(
          padding: bodyPaddingLeftRight,
          child: ComponentTextBtn('수정', () {
            if(_formKey.currentState!.saveAndValidate()) {
              ReviewRequest request = ReviewRequest(
                _formKey.currentState!.fields['name']!.value,
                _formKey.currentState!.fields['title']!.value,
                _formKey.currentState!.fields['imgName']!.value,
                _formKey.currentState!.fields['content']!.value,
              );
              _putReview(request);
            }
          }),
        ),
        const ComponentMarginVertical(
          enumSize: EnumSize.small,
        )
      ],
    );
  }
}