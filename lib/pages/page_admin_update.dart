import 'package:bot_toast/bot_toast.dart';
import 'package:daligo_admin_app/components/common/component_appbar_actions.dart';
import 'package:daligo_admin_app/components/common/component_appbar_popup.dart';
import 'package:daligo_admin_app/components/common/component_custom_loading.dart';
import 'package:daligo_admin_app/components/common/component_margin_vertical.dart';
import 'package:daligo_admin_app/components/common/component_notification.dart';
import 'package:daligo_admin_app/components/common/component_text_btn.dart';
import 'package:daligo_admin_app/config/config_form_validator.dart';
import 'package:daligo_admin_app/config/config_style.dart';
import 'package:daligo_admin_app/enums/enum_size.dart';
import 'package:daligo_admin_app/model/admin_update_request.dart';
import 'package:daligo_admin_app/pages/page_main.dart';
import 'package:daligo_admin_app/repository/repo_member.dart';
import 'package:daligo_admin_app/styles/style_form_decoration.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class PageAdminUpdate extends StatefulWidget {
  const PageAdminUpdate({super.key});

  @override
  State<PageAdminUpdate> createState() => _PageAdminUpdateState();
}

class _PageAdminUpdateState extends State<PageAdminUpdate> {
  final _formKey = GlobalKey<FormBuilderState>();
  int? option;

  var maskPhoneNumber = MaskTextInputFormatter(
      mask: '###-####-####',
      filter: { "#": RegExp(r'[0-9]') },
      type: MaskAutoCompletionType.lazy
  );

  Future<void> _memberUpdate(AdminUpdateRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMember().adminUpdate(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '수정 완료',
        subTitle: '수정을 완료하였습니다.',
      ).call();

      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => const PageMain()),
              (route) => false);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '수정 실패',
        subTitle: '입력값을 확인해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(
        title: '정보 수정',
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return ListView(
      children: [
        const ComponentMarginVertical(
          enumSize: EnumSize.mid,
        ),
        Container(
          padding: const EdgeInsets.only(
            bottom: 10,
            left: 20,
            right: 20,
          ),
          child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'name',
                    decoration: StyleFormDecoration().getInputDecoration('이름'),
                    maxLength: 20,
                    keyboardType: TextInputType.text,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(2, errorText: formErrorMinLength(2)),
                      FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                    ]),
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  decoration: formBoxDecoration,
                  padding: bodyPaddingAll,
                  child: FormBuilderTextField(
                    name: 'phoneNumber',
                    inputFormatters: [maskPhoneNumber],
                    decoration: StyleFormDecoration().getInputDecoration('연락처'),
                    maxLength: 13,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(13, errorText: formErrorMinLength(13)),
                      FormBuilderValidators.maxLength(13, errorText: formErrorMaxLength(13)),
                    ]),
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(
          padding: bodyPaddingLeftRight,
          child: ComponentTextBtn('수정', () {
            if(_formKey.currentState!.saveAndValidate()) {
              AdminUpdateRequest request = AdminUpdateRequest(
                _formKey.currentState!.fields['name']!.value,
                _formKey.currentState!.fields['phoneNumber']!.value,
              );
              _memberUpdate(request);
            }
          }),
        ),
        const ComponentMarginVertical(
          enumSize: EnumSize.small,
        ),
        const ComponentMarginVertical(
          enumSize: EnumSize.small,
        ),
      ],
    );
  }
}
