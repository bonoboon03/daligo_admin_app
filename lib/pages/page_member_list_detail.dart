import 'package:daligo_admin_app/components/common/component_appbar_popup.dart';
import 'package:daligo_admin_app/components/common/component_margin_vertical.dart';
import 'package:daligo_admin_app/config/config_color.dart';
import 'package:daligo_admin_app/config/config_size.dart';
import 'package:daligo_admin_app/enums/enum_size.dart';
import 'package:daligo_admin_app/model/member/member_result.dart';
import 'package:daligo_admin_app/repository/repo_member.dart';
import 'package:flutter/material.dart';

class PageMemberListDetail extends StatefulWidget {
  const PageMemberListDetail({super.key, required this.id, required this.title});

  final int id;
  final String title;

  @override
  State<PageMemberListDetail> createState() => _PageMemberListDetailState();
}

class _PageMemberListDetailState extends State<PageMemberListDetail> {
  String memberGroup = "";
  String username = "";
  String name = "";
  String phoneNumber = "";
  String licenseNumber = "";
  double point = 0;
  String dateCreate = "";
  String isEnabled = "";


  Future<void> _getMember() async {
    MemberResult result = await RepoMember().getMember(widget.id);

    setState(() {
      memberGroup = result.data.memberGroup;
      username = result.data.username;
      name = result.data.name;
      phoneNumber = result.data.phoneNumber;
      licenseNumber = result.data.licenseNumber;
      point = result.data.point;
      dateCreate = result.data.dateCreate;
      isEnabled = result.data.isEnabled;
    });
  }

  @override
  void initState() {
    super.initState();
    _getMember();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(
        title: widget.title,
      ),
      body: Container(
        padding: const EdgeInsets.all(20),
        margin: const EdgeInsets.only(bottom: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: colorLightGray),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "그룹",
                  style: const TextStyle(
                    fontSize: fontSizeBig,
                    wordSpacing: 1.5,
                    height: 1.3,
                  ),
                ),
                Text(
                  memberGroup,
                  style: const TextStyle(
                    fontSize: fontSizeBig,
                    wordSpacing: 1.5,
                    height: 1.3,
                  ),
                ),
              ],
            ),
            const ComponentMarginVertical(enumSize: EnumSize.mid),
            Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "아이디",
                  style: const TextStyle(
                    fontSize: fontSizeBig,
                    wordSpacing: 1.5,
                    height: 1.3,
                  ),
                ),
                Text(
                  username,
                  style: const TextStyle(
                    fontSize: fontSizeBig,
                    wordSpacing: 1.5,
                    height: 1.3,
                  ),
                ),
              ],
            ),
            const ComponentMarginVertical(enumSize: EnumSize.mid),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "연락처",
                  style: const TextStyle(
                    fontSize: fontSizeBig,
                    wordSpacing: 1.5,
                    height: 1.3,
                  ),
                ),
                Text(
                  phoneNumber,
                  style: const TextStyle(
                    fontSize: fontSizeBig,
                    wordSpacing: 1.5,
                    height: 1.3,
                  ),
                ),
              ],
            ),
            const ComponentMarginVertical(enumSize: EnumSize.mid),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "면허증 번호",
                  style: const TextStyle(
                    fontSize: fontSizeBig,
                    wordSpacing: 1.5,
                    height: 1.3,
                  ),
                ),
                Text(
                  licenseNumber,
                  style: const TextStyle(
                    fontSize: fontSizeBig,
                    wordSpacing: 1.5,
                    height: 1.3,
                  ),
                ),
              ],
            ),
            const ComponentMarginVertical(enumSize: EnumSize.mid),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "포인트",
                  style: const TextStyle(
                    fontSize: fontSizeBig,
                    wordSpacing: 1.5,
                    height: 1.3,
                  ),
                ),
                Text(
                  "$point포인트",
                  style: const TextStyle(
                    fontSize: fontSizeBig,
                    wordSpacing: 1.5,
                    height: 1.3,
                  ),
                ),
              ],
            ),
            const ComponentMarginVertical(enumSize: EnumSize.mid),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "가입일",
                  style: const TextStyle(
                    fontSize: fontSizeBig,
                    wordSpacing: 1.5,
                    height: 1.3,
                  ),
                ),
                Text(
                  dateCreate,
                  style: const TextStyle(
                    fontSize: fontSizeBig,
                    wordSpacing: 1.5,
                    height: 1.3,
                  ),
                ),
              ],
            ),
            const ComponentMarginVertical(enumSize: EnumSize.mid),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "활성화 여부",
                  style: const TextStyle(
                    fontSize: fontSizeBig,
                    wordSpacing: 1.5,
                    height: 1.3,
                  ),
                ),
                Text(
                  "$isEnabled",
                  style: const TextStyle(
                    fontSize: fontSizeBig,
                    wordSpacing: 1.5,
                    height: 1.3,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
