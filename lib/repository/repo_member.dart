import 'package:daligo_admin_app/functions/token_lib.dart';
import 'package:daligo_admin_app/model/change_password_request.dart';
import 'package:daligo_admin_app/model/common_result.dart';
import 'package:daligo_admin_app/model/find_password_request.dart';
import 'package:daligo_admin_app/model/find_username_request.dart';
import 'package:daligo_admin_app/model/find_username_result.dart';
import 'package:daligo_admin_app/model/member/member_create_request.dart';
import 'package:daligo_admin_app/model/member/member_list_result.dart';
import 'package:daligo_admin_app/model/admin_update_request.dart';
import 'package:daligo_admin_app/model/member/member_result.dart';
import 'package:dio/dio.dart';
import 'package:daligo_admin_app/config/config_api.dart';
import 'package:daligo_admin_app/model/login_request.dart';
import 'package:daligo_admin_app/model/login_result.dart';

class RepoMember {
  Future<LoginResult> doLogin(LoginRequest loginRequest) async {
    const String baseUrl = '$apiUri/member/login/app/admin';

    Dio dio = Dio();

    final response = await dio.post(
        baseUrl,
        data: loginRequest.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return LoginResult.fromJson(response.data);
  }

  Future<CommonResult> setMember(MemberCreateRequest request) async {
    const String baseUrl = '$apiUri/member/data';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.post(
        baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              print(status);
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

  Future<MemberListResult> getList({int page = 1}) async {
    const String baseUrl = '$apiUri/member/all';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(baseUrl.replaceAll('{page}', page.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return MemberListResult.fromJson(response.data);
  }

  Future<CommonResult> adminUpdate(AdminUpdateRequest request) async {
    const String baseUrl = '$apiUri/member/admin-update';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.put(
        baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return CommonResult.fromJson(response.data);
  }

  Future<CommonResult> changePassword(ChangePasswordRequest request) async {
    const String baseUrl = '$apiUri/member/password';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.put(
        baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return CommonResult.fromJson(response.data);
  }

  Future<FindUsernameResult> findUsername(FindUsernameRequest request) async {
    const String baseUrl = '$apiUri/member/find-username';

    Dio dio = Dio();

    final response = await dio.post(
        baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return FindUsernameResult.fromJson(response.data);
  }

  Future<CommonResult> findPassword(FindPasswordRequest request) async {
    const String baseUrl = '$apiUri/member/find-password';

    Dio dio = Dio();

    final response = await dio.post(
        baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return CommonResult.fromJson(response.data);
  }

  Future<MemberResult> getMember(int id) async {
    const String baseUrl = '$apiUri/member/member?id={id}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(baseUrl.replaceAll('{id}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return MemberResult.fromJson(response.data);
  }
}