import 'package:daligo_admin_app/config/config_api.dart';
import 'package:daligo_admin_app/functions/token_lib.dart';
import 'package:daligo_admin_app/model/common_result.dart';
import 'package:daligo_admin_app/model/kick_board/kick_board_result.dart';
import 'package:daligo_admin_app/model/review/review_list_result.dart';
import 'package:daligo_admin_app/model/review/review_request.dart';
import 'package:daligo_admin_app/model/review/review_result.dart';
import 'package:dio/dio.dart';

class RepoReview {
  Future<CommonResult> setReview(ReviewRequest reviewRequest) async {
    const String baseUrl = '$apiUri/review/data';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.post(
        baseUrl,
        data: reviewRequest.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

  Future<ReviewListResult> getList({int page = 1}) async {
    const String baseUrl = '$apiUri/review/all';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(baseUrl.replaceAll('{page}', page.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return ReviewListResult.fromJson(response.data);
  }

  Future<ReviewResult> getReview(int id) async {
    const String baseUrl = '$apiUri/review/review?id={id}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(baseUrl.replaceAll('{id}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              print(status);
              return status == 200;
            }
        )
    );
    return ReviewResult.fromJson(response.data);
  }

  Future<CommonResult> putReview(int id, ReviewRequest reviewRequest) async {
    const String baseUrl = '$apiUri/review/{id}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.put(
        baseUrl.replaceAll('{id}', id.toString()),
        data: reviewRequest.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

  Future<CommonResult> delReview(int id) async {
    const String baseUrl = '$apiUri/review/{id}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.delete(
        baseUrl.replaceAll('{id}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }
}